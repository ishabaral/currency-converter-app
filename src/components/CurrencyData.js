import React, { useEffect, useState } from "react";

function CurrencyData(props) {
  const [currencyOptions, setCurrencyOptions] = useState([]);
  const [fromCurrency, setFromCurrency] = useState();
  const [toCurrency, setToCurrency] = useState();
  const [exchangeRate, setExchangeRate] = useState();
  const [amount, setAmount] = useState(1);
  const [amountInFromCurrency, setAmountInFromCurrency] = useState(true);

  const BASE_URL = "http://api.exchangeratesapi.io/v1/latest";
  const access_key = "f46a5ce309ce3f9c3354b974be4535b0";

  let fromAmount, toAmount;

  if (amountInFromCurrency) {
    fromAmount = amount;
    toAmount = amount * exchangeRate || 0;
  } else {
    toAmount = amount;
    fromAmount = amount / exchangeRate;
  }

  const fromCurrencyChange = (e) => {
    setFromCurrency(e.target.value);
  };
  const toCurrencyChange = (e) => {
    setToCurrency(e.target.value);
  };

  const handleFromAmountChange = (e) => {
    setAmount(e.target.value);
    setAmountInFromCurrency(true);
  };
  const handleToAmountChange = (e) => {
    setAmount(e.target.value);
    setAmountInFromCurrency(false);
  };

  useEffect(() => {
    fetch(`${BASE_URL}?access_key=${access_key}`)
      .then((response) => response.json())
      .then((data) => {
        const firstCurrency = Object.keys(data.rates)[0];
        setCurrencyOptions([data.base, ...Object.keys(data.rates)]);
        setFromCurrency(data.base);
        setToCurrency(firstCurrency);
        setExchangeRate(data.rates[firstCurrency]);
      });
  }, []);

  // useEffect(() => {
  //   if (fromCurrency === toCurrency && fromCurrency != null) {
  //     setExchangeRate(1);
  //   } else if (fromCurrency != null && toCurrency != null) {
  //     fetch(`${BASE_URL}?access_key=${access_key}&base=${fromCurrency}`)
  //       .then((response) => response.json())
  //       .then((data) => {
  //         if (Object.keys(data.rates).length > 0) {
  //           debugger;
  //           const filteredCurrency = Object.keys(data.rates).find(
  //             (currency) => currency === toCurrency
  //           );
  //           console.log(filteredCurrency, data.rates[filteredCurrency]);
  //           setExchangeRate(data.rates[filteredCurrency]);
  //         } else {
  //           setExchangeRate(0);
  //         }
  //       });
  //   }
  // }, [fromCurrency, toCurrency]);

  return (
    <div>
      {props.children(
        currencyOptions,
        fromCurrency,
        toCurrency,
        toCurrencyChange,
        fromCurrencyChange,
        fromAmount,
        toAmount,
        handleFromAmountChange,
        handleToAmountChange
      )}
    </div>
  );
}

export default CurrencyData;
