import React from "react";

function InputCurrency(props) {
  const {
    currencyOptions,
    selectedCurrency,
    onCurrencyChange,
    amount,
    onAmountChange,
  } = props;

  return (
    <div>
      <input type="text" onChange={onAmountChange} value={amount} />
      <select onChange={onCurrencyChange} value={selectedCurrency}>
        {currencyOptions.map((currencyOption) => (
          <option key={currencyOption} value={currencyOption}>
            {currencyOption}
          </option>
        ))}
      </select>
    </div>
  );
}

export default InputCurrency;
