import React from "react";
import "./App.css";
import CurrencyData from "./components/CurrencyData";
import InputCurrency from "./components/InputCurrency";

function App() {
  return (
    <div className="App">
      <h1>Convert Currency</h1>
      <CurrencyData>
        {(
          currencyOptions,
          fromCurrency,
          toCurrency,
          toCurrencyChange,
          fromCurrencyChange,
          fromAmount,
          toAmount,
          handleFromAmountChange,
          handleToAmountChange
        ) => {
          return (
            <React.Fragment>
              <InputCurrency
                currencyOptions={currencyOptions}
                selectedCurrency={fromCurrency}
                onCurrencyChange={(e) => fromCurrencyChange(e)}
                amount={fromAmount}
                onAmountChange={(e) => handleFromAmountChange(e)}
              />
              <p>=</p>
              <InputCurrency
                currencyOptions={currencyOptions}
                selectedCurrency={toCurrency}
                onCurrencyChange={(e) => toCurrencyChange(e)}
                amount={toAmount}
                onAmountChange={(e) => handleToAmountChange(e)}
              />
            </React.Fragment>
          );
        }}
      </CurrencyData>
    </div>
  );
}

export default App;
